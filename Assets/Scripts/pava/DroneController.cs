﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum DroneState{
	fly_to_camera,
	idle_flight,
	show_off,
	return_to_show_off,
	dekalb,
	vt_pro,
	refugio_max,
	monsoy,
	intacta,
	idle_still,
	pulverizer,
	NPC,
	camera_idle
}

public class DroneController : MonoBehaviour 
{
	private BezierCurve _tempararySpline;
	private BezierCurve _currentSpline;
	private BezierSpline _idleLoop;
	private BezierSpline _showOff;

	private DroneState _currentState;
	
	private bool _setupReturn = false;

	private float _splineProgress = 0f;

	public float logoTimeOnScreen = 3f;
	public float idleTimeOnScreen = 3f;
	public float droneSpeed = 0.1f;
	public float idleSpeed = 0.05f;
	public float showOffSpeed = 0.1f;

	public CustomEvent OnFlyToCameraComplete;
	public CustomEvent OnReturnToShowOffComplete;
	public CustomEvent OnGoToComplete;

	public Transform dekalbTransform;
	public Transform vtProTransform;
	public Transform monsoyTransform;
	public Transform intactaTransform;
	public Transform refugioMaxTransform;
	public Transform pulverizerTransform;
	public Transform NPCTransform;
	 
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(2f);
		//InitializePulverizer();
	}

	private void Update()
	{
		switch(_currentState)
		{
			case(DroneState.fly_to_camera):
				FlyToCamera();
			break;

			case(DroneState.idle_flight):
				IdleFlight();
			break;

			case(DroneState.show_off):
				ShowOff();
			break;

			case(DroneState.return_to_show_off):
				ReturnToShowOff();
			break;

			case(DroneState.dekalb):
				DroneGoTo(dekalbTransform, false);
			break;

			case(DroneState.vt_pro):
				DroneGoTo(vtProTransform, false);
			break;

			case(DroneState.refugio_max):
				DroneGoTo(refugioMaxTransform, false);
			break;

			case(DroneState.monsoy):
				DroneGoTo(monsoyTransform, false);
			break;

			case(DroneState.intacta):
				DroneGoTo(intactaTransform, false);
			break;

			case(DroneState.idle_still):
				Hover();
			break;

			case(DroneState.pulverizer):
				DroneGoTo(pulverizerTransform);
			break;

			case(DroneState.NPC):
				DroneGoTo(NPCTransform);
			break;
		}
	}

	public void CreateSplineToPosition(Vector3 targetPosition)
	{
		if(_tempararySpline != null)
		{
			Destroy(_tempararySpline.gameObject);
			_tempararySpline = null;
		}
		GameObject newborn = new GameObject("{TEMP} - TemporarySpline");
		_tempararySpline = newborn.AddComponent<BezierCurve>();

		_tempararySpline.transform.position = transform.position;
		_tempararySpline.Reset();

		_tempararySpline.SetFinalPosition(_tempararySpline.transform.InverseTransformPoint(targetPosition));

		SetBezierSpline(_tempararySpline);
	}

	public void SetDroneState(DroneState state)
	{
		_currentState = state;
		OnEnterState();
	}

	private void OnEnterState()
	{
		if(_currentState == DroneState.camera_idle)
		{
			transform.GetChild(0).gameObject.SetActive(false);
		}else{
			transform.GetChild(0).gameObject.SetActive(true);
		}
	}

	public void SetBezierSpline(BezierCurve spline)
	{
		_splineProgress = 0f;
		_currentSpline = spline;
	}

#region INITIALIZATION FUNCTIONS
	public void InitializeFlyToCamera()
	{
		Transform target = GameObject.FindGameObjectWithTag("CameraPosition").transform;
		CreateSplineToPosition(target.position);
		SetDroneState(DroneState.fly_to_camera);
	}

	public void InitializeReturnToShowOff()
	{
		SetDroneState(DroneState.return_to_show_off);
	}

	public void InitializeShowOff()
	{
		SetDroneState(DroneState.show_off);
	}

	public void InitializeDekalb()
	{
		Debug.Log("Dekalb");
		SetDroneState(DroneState.dekalb);
	}

	public void InitializeVTPro()
	{
		Debug.Log("VTPro");
		SetDroneState(DroneState.vt_pro);
	}

	public void InitializeRefugioMax()
	{
		Debug.Log("RefugioMax");
		SetDroneState(DroneState.refugio_max);
	}

	public void InitializeMonsoy()
	{
		Debug.Log("Monsoy");
		SetDroneState(DroneState.monsoy);
	}

	public void InitializeIntacta()
	{
		Debug.Log("Intacta");
		SetDroneState(DroneState.intacta);
	}

	public void InitializePulverizer()
	{
		Debug.Log("Pulverizer");
		SetDroneState(DroneState.pulverizer);
	}

	public void InitializeNPC()
	{
		Debug.Log("NPC");
		SetDroneState(DroneState.NPC);
	}
#endregion

#region STATE FUNCTIONS
	private bool _setupGoTo = false;
	private void DroneGoTo(Transform target, bool dispatchComplete = true)
	{
		if(!_setupGoTo)
		{
			CreateSplineToPosition(target.position);
			_setupGoTo = true;
		}

		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(_currentSpline.GetDirection(_splineProgress));
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				_setupHover = false;
				SetDroneState(DroneState.idle_still);
				_setupGoTo = false;
				if(dispatchComplete)
				{
					OnGoToComplete.Invoke();
				}
				return;
			}
		}
	}

	private void ReturnToShowOff()
	{
		if(!_setupReturn)
		{
			SetupReturn();
			_setupReturn = true;
			return;
		}

		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(_currentSpline.GetDirection(_splineProgress));
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				SetDroneState(DroneState.show_off);
				_setupReturn = false;
				OnReturnToShowOffComplete.Invoke();
				return;
			}
		}
	}

	private void SetupReturn()
	{
		if(_showOff == null)
		{
			_showOff = GameObject.FindGameObjectWithTag("ShowOffLoop").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		CreateSplineToPosition(_showOff.GetPoint(0f));
	}

	private void FlyToCamera()
	{
		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				OnFlyToCameraComplete.Invoke();
				SetDroneState(DroneState.camera_idle);
			}
		}
	}
	
	private void IdleFlight()
	{
		if(_idleLoop == null)
		{
			_idleLoop = GameObject.FindGameObjectWithTag("IdleLoop").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		_splineProgress += Time.deltaTime * idleSpeed;
		if (_splineProgress > 1f) 
		{
			_splineProgress = 0f;
		}

		Vector3 position = _idleLoop.GetPoint(_splineProgress);
		transform.position = Vector3.Lerp (transform.position, position, _splineProgress);
		
		var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);
	}

	private void ShowOff()
	{
		if(_showOff == null)
		{
			_showOff = GameObject.FindGameObjectWithTag("ShowOffLoop").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		_splineProgress += Time.deltaTime * showOffSpeed;
		if (_splineProgress > 1f) {
			_splineProgress = 0f;
			return;
		}

		Vector3 position = _showOff.GetPoint(_splineProgress);
        transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

		transform.LookAt(position + _showOff.GetDirection(_splineProgress));
	}

	private bool _setupHover = false;
	private BezierSpline _hoverLoop;
	private void Hover()
	{
		if(!_setupHover)
		{
			_hoverLoop = GameObject.FindGameObjectWithTag("DroneHover").GetComponent<BezierSpline>();
			_hoverLoop.transform.localPosition = transform.localPosition;
			_splineProgress = 0f;
			_setupHover = true;
		}

		_splineProgress += Time.deltaTime * showOffSpeed;
		if (_splineProgress > 1f) {
			_splineProgress = 0f;
			return;
		}

		Vector3 position = _hoverLoop.GetPoint(_splineProgress);
        transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

		transform.LookAt(position + _hoverLoop.GetDirection(_splineProgress));
	}
#endregion
}
