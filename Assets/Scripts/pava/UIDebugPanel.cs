﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugPanel : MonoBehaviour 
{
	public GameObject passwordPanel;
	public InputField passwordInput;

	public string password = "1-2-3";
	public string[] passwordSplit;
	
	private List<string> _input;

	private void Awake()
	{
		passwordSplit = password.Split('-');
		_input = new List<string>();
	}
	
	private void OnEnable() 
	{
		passwordPanel.SetActive(true);	
		Clear();
	}

	public void Close()
	{
		gameObject.SetActive(false);
	}

	public void InputPassword(string value)
	{
		passwordInput.text = passwordInput.text + "*";
		_input.Add(value);
		CheckInput();
	}

	public void Clear()
	{
		_input = new List<string>();
		passwordInput.text = "";
	}

	private void CheckInput()
	{
		int inputCount = _input.Count;
		int passCount = passwordSplit.Length;

		if(inputCount != passCount)
		{
			return;
		}

		bool[] check = new bool[_input.Count];
		for(int i = 0; i < _input.Count; i++)
		{
			string[] split = _input[i].Split('-');
			foreach(string value in split)
			{
				if(split.Contains(passwordSplit[i]))
				{
					check[i] = true;
				}
			}
		}

		bool isCorrect = true;
		foreach(bool current in check)
		{
			if(!current)
			{
				isCorrect = false;
				break;
			}
		}

		if(isCorrect)
		{
			Debug.Log("PASSWORD IS CORRECT!");
			passwordPanel.SetActive(false);
		}
	}
}