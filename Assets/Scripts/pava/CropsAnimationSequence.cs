﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CropsAnimationSequence : MonoBehaviour 
{
	public bool isDebug = false;

	private void Start()
	{
		if(isDebug)
		{
			PlayAnimationSequence();
		}
	}

	public void PlayAnimationSequence()
	{
		PlantPoisitioner[] plants = gameObject.GetComponentsInChildren<PlantPoisitioner>();

		foreach (PlantPoisitioner current in plants)
		{
			current.PlayAnimationSequence();
		}
	}
}
