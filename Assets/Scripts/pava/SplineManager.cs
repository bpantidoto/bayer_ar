﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplineManager : MonoBehaviour 
{
	public DroneController droneController;

	private BezierCurve _currentSpline;

	private void Start()
	{
		CreateCameraSpline();
	}

	public void CreateCameraSpline()
	{
		GameObject newborn = new GameObject("{TEMP} - CameraSpline");
		_currentSpline = newborn.AddComponent<BezierCurve>();

		_currentSpline.transform.position = droneController.transform.position;
		_currentSpline.Reset();

		Transform target = GameObject.FindGameObjectWithTag("CameraPosition").transform;
		Vector3 cameraPosition = target.position;

		_currentSpline.SetFinalPosition(cameraPosition);

		droneController.SetBezierSpline(_currentSpline);
		droneController.SetDroneState(DroneState.fly_to_camera);
	}
}
