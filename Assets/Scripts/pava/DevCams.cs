﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DevCams : MonoBehaviour 
{
	public GameObject arCamera;
	public GameObject devCamera;
	public bool useDevCams;

#if UNITY_EDITOR
	private void Awake()
	{	
		if(useDevCams)
		{
			arCamera.SetActive(false);
			devCamera.SetActive(true);

		}
	}
#endif
}
