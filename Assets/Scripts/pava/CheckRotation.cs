﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckRotation : MonoBehaviour 
{
	public Text textfield;
	public Transform[] targets;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		string final = "";
		foreach (Transform current in targets)
		{
			final = final + current.name + ":"+ current.rotation.ToString() + "\n"+ current.position.ToString()+ "\n________";	
		}

		textfield.text = final;
	}
}
