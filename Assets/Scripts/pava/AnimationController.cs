﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour 
{
	public AnimationScale[] animations;

	private void Start()
	{
		animations = gameObject.GetComponentsInChildren<AnimationScale>();
	}

	public void Animate()
	{
		foreach(AnimationScale current in animations)
		{
			current.AnimateIn();
		}
	}

	public void Reset()
	{
		foreach(AnimationScale current in animations)
		{
			current.Reset();
		}
	}
}
