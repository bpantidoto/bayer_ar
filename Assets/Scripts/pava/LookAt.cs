﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour 
{
	public bool simpleLookAt = true;

	public float offset = 0f;

	private Quaternion _initialRotation;
	private Quaternion _finalRotation;


	private void Awake()
	{
		_initialRotation = transform.rotation;
		transform.LookAt(Camera.main.transform);
		_finalRotation = transform.rotation;
		transform.rotation = _initialRotation;
	}

	void Update () 
	{
		if(simpleLookAt)
		{
			transform.LookAt(Camera.main.transform);
		}else{
			transform.LookAt(Camera.main.transform);
			_finalRotation = transform.rotation;
			transform.rotation = Quaternion.Euler(0f, _finalRotation.y - offset + Camera.main.transform.rotation.eulerAngles.y, 0f);
		}
	}
}
