﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FarmRotationManager : MonoBehaviour 
{
	public float protectionValue = 150f;
	public float technologyValue = 275f;
	public float seedsValue = 350f;
	public float animationTime = 1.5f;

	private Quaternion _finalRotation;
	private Quaternion _currentRotation;

	public CustomEvent OnProtectionComplete;
	public CustomEvent OnTechnologyComplete;
	public CustomEvent OnSeedsComplete;
	public CustomEvent OnResetComplete;

#if UNITY_EDITOR
	public float debugOffset = 0f;
	public bool useDebugOffset = false;
	private void Update()
	{
		if(useDebugOffset)
		{
			transform.DORotate(new Vector3(0.5f, _finalRotation.y - debugOffset + Camera.main.transform.rotation.eulerAngles.y, 0f), 0f);
		}
	}
#endif
	
	public void SetInitialRotation()
	{
		LookAtCamera();
		transform.DORotate(new Vector3(0.5f, _finalRotation.y - technologyValue + Camera.main.transform.rotation.eulerAngles.y, 0f), 0f);
	}

	public void ResetRotation()
	{
		transform.DOKill();
		transform.DORotate(new Vector3(0f, _finalRotation.y - technologyValue + Camera.main.transform.rotation.eulerAngles.y, 0f), animationTime).OnComplete(delegate{
			OnResetComplete.Invoke();
		});
	}

	public void RotateToProtectionQuadrant()
	{
		LookAtCamera();		

		transform.DORotate(new Vector3(0f, _finalRotation.y - protectionValue + Camera.main.transform.rotation.eulerAngles.y, 0f), animationTime).OnComplete(delegate{
			OnProtectionComplete.Invoke();
		});
	}

	public void RotateToTechnologyQuadrant()
	{
		LookAtCamera();

		transform.DORotate(new Vector3(0f, _finalRotation.y - technologyValue + Camera.main.transform.rotation.eulerAngles.y, 0f), animationTime).OnComplete(delegate{
			OnTechnologyComplete.Invoke();
		});
	}

	public void RotateToSeedsQuadrant()
	{
		LookAtCamera();

		transform.DORotate(new Vector3(0f, _finalRotation.y - seedsValue + Camera.main.transform.rotation.eulerAngles.y, 0f), animationTime).OnComplete(delegate{
			OnSeedsComplete.Invoke();
		});
	}

	private void LookAtCamera()
	{
		_currentRotation = transform.rotation;
		transform.LookAt(Camera.main.transform);
		_finalRotation = transform.rotation;
		transform.rotation = _currentRotation;
	}
}
