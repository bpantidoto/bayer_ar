﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NPCController : MonoBehaviour 
{
	private Vector3 _initialScale;
	public float animationTime = 0.5f;
	public Ease easing = Ease.OutQuad;

	public GameObject NPCtablet;

	public CustomEvent onAnimationComplete;


	private void Awake()
	{
		Setup();
	}

	private void Setup()
	{
		_initialScale = transform.localScale;
		transform.localScale = Vector3.zero;
	}

	public void ResetNPC()
	{
		transform.localScale = Vector3.zero;
		transform.DOKill();
	}

	public void IntroduceNPC()
	{
		transform.DOScale(_initialScale, animationTime).SetEase(easing).OnComplete(delegate{
			NPCtablet.SetActive(false);
			onAnimationComplete.Invoke();
		});
	}
}
