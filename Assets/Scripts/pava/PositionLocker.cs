﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionLocker : MonoBehaviour 
{
	private Transform _cameraTransform;
	public float distance;
	public float moveSpeed;

	public bool shouldUpdate = false;

	private void Awake()
	{
		_cameraTransform = Camera.main.transform;
	}

	private void Update () 
	{
		transform.position = _cameraTransform.position + (_cameraTransform.up * distance);
	}
}
