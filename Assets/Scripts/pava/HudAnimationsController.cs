﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HudAnimationsController : MonoBehaviour 
{
	public Text textField;
	public float textFieldDelay;

	public Animator[] animators;
	public float[] triggerDelays;

	public string playAnim = "PlayAnimation";
	public string resetAnim = "Reset";

	public void PlayAnimation()
	{
		for(int i = 0; i<animators.Length; i++)
		{
			StartCoroutine(PlayAnim(animators[i], triggerDelays[i]));
		}
	}

	private void ResetText(float duration = 0f)
	{
		if(textField != null)
		{
			textField.text = "";
			textField.DOFade(0f, duration);
		}
	}

	public void SetText(string text)
	{
		if(textField != null)
		{
			textField.text = "";
			textField.DOFade(0f,0f);
			textField.DOFade(1f, 0.5f).SetDelay(textFieldDelay).OnComplete(delegate{
				textField.DOText(text, 1f, true, ScrambleMode.All);
			});
		}
	}

	private IEnumerator PlayAnim(Animator target, float delay)
	{
		target.GetComponent<Image>().DOFade(1f, 0f);
		yield return new WaitForSeconds(delay);
		target.SetTrigger(playAnim);
	}

	public void Reset()
	{
		//Debug.Log("Reset: "+gameObject.name);
		StopAllCoroutines();
		textField.DOKill();
		ResetText(0.5f);
		foreach (Animator current in animators)
		{
			current.GetComponent<Image>().DOFade(0f, 0.5f).OnComplete(delegate{
				current.SetTrigger("Reset");
			});
		}
	}
}
