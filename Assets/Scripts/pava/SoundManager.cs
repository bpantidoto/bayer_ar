﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{	
	public bool MusicOn = true;
	public bool SfxOn = true;
	
	[Range(0,1)]
	public float MusicVolume = 0.3f;
	
	[Range(0,1)]
	public float SfxVolume = 1f;

	[Range(0,1)]
	public float SfxLowerVolume = 1f;

	[Range(0,1)]
	public float StingerVolume = 0.5f;

	public AudioSource backgroundMusic;
	private AudioSource _currentPlayingSound;	

	private bool _isMusicPlaying = false;
		
	public void PlayBackgroundMusic()
	{
		if(_isMusicPlaying)
			return;
		if (!MusicOn)
			return;
		
		backgroundMusic.volume = MusicVolume;
		backgroundMusic.loop = true;
		backgroundMusic.Play();
		_isMusicPlaying = true;		
	}	

	public void StopBackGroundMusic()
	{
		if (backgroundMusic != null)
		{
			_isMusicPlaying = false;
			backgroundMusic.Stop();
		}
	}

	public void StopCurrentPlayingSound()
	{
		if(_currentPlayingSound != null)
		{
			_currentPlayingSound.Stop();
			Destroy(_currentPlayingSound);
		}
	}

	public AudioSource PlaySound(AudioClip sfx, Vector3 location, bool shouldDestroyAfterPlay=true)
	{
		if (!SfxOn)
			return null;

		StopCurrentPlayingSound();

		_currentPlayingSound = gameObject.AddComponent<AudioSource>() as AudioSource; 
		_currentPlayingSound.clip = sfx; 
		_currentPlayingSound.volume = SfxVolume;
		_currentPlayingSound.Play(); 

		if (shouldDestroyAfterPlay)
		{
			Destroy(_currentPlayingSound, sfx.length);
		}
		
		return _currentPlayingSound;
	}

	public AudioSource PlayLoop(AudioClip Sfx, Vector3 Location)
	{
		if (!SfxOn)
			return null;
		
		GameObject temporaryAudioHost = new GameObject("TempAudio");
		temporaryAudioHost.transform.position = Location;
		
		AudioSource audioSource = temporaryAudioHost.AddComponent<AudioSource>() as AudioSource; 
		audioSource.clip = Sfx; 
		audioSource.volume = SfxVolume;
		audioSource.loop=true;
		audioSource.Play(); 
		
		return audioSource;
	}
}