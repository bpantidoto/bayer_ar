﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShitExplosion : MonoBehaviour 
{
	public GameObject prefab;
	private List<RandomItem> _items;
	
	public ForceMode forcemode;

	public float duration = 5f;
	public float lifeSpan = 5f;
	public float interval = 0.5f;

	private float _currentTime;

	public Vector3 vectorForceMin = new Vector3(-1500f, 2000f, -1000f);
	public Vector3 vectorForceMax = new Vector3(1000f, 2000f, -1000f);

	public Vector3 angularVectorForceMin = new Vector3(1f, 1f, 1f);
	public Vector3 angularVectorForceMax = new Vector3(2f, 2f, 2f);

	private Vector3 _angularVector;

	private bool _shouldUpdate = false;

	public CustomEvent OnExplosionComplete;

	private void Start()
	{
		_angularVector = new Vector3(Random.Range(angularVectorForceMin.x, angularVectorForceMax.x), Random.Range(angularVectorForceMin.y, angularVectorForceMax.y), Random.Range(angularVectorForceMin.z, angularVectorForceMax.z));
		//StartExplosion();
	}

	private IEnumerator CreateItemWithDelay(float delay)
	{
		yield return new WaitForSeconds(delay);
		CreateItem();
	}

	private void CreateItem()
	{
		GameObject newborn = GameObject.Instantiate(prefab, transform.position, Quaternion.identity) as GameObject;
		newborn.transform.SetParent(transform.parent);
		newborn.transform.position = transform.position;

		RandomItem item = newborn.GetComponent<RandomItem>();
		item.PlayAnimation();

		newborn.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(vectorForceMin.x, vectorForceMax.x), Random.Range(vectorForceMin.y, vectorForceMax.y), Random.Range(vectorForceMin.z, vectorForceMax.z)), forcemode);

		newborn.GetComponent<Rigidbody>().angularVelocity = _angularVector;

		GameObject.Destroy(newborn, lifeSpan);
	}

	private bool _canExplode = true;
	public void StartExplosion()
	{
		if(_canExplode)
		{
			_shouldUpdate = true;
			_canExplode = false;
		}
	}

	private float _currentInverval = 0f;	
	private void Update()
	{
		if(_shouldUpdate)
		{
			_currentTime += Time.deltaTime;

			_currentInverval += Time.deltaTime;
			if(_currentInverval >= interval)
			{
				_currentInverval = 0f;
				CreateItem();
			}

			if(_currentTime >= duration)
			{
				_shouldUpdate = false;
				OnExplosionComplete.Invoke();
			}
		}
	}
}
