﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public enum SateliteState{
	fly_to_camera,
	idle_flight,
	show_off,
	idle_still,
	return_to_show_off,
	camera_idle
}

public class SateliteController : MonoBehaviour 
{
	private BezierCurve _tempararySpline;
	private BezierCurve _currentSpline;
	private BezierSpline _idleLoop;
	private BezierSpline _showOff;

	private SateliteState _currentState;
	
	private bool _setupReturn = false;

	private float _splineProgress = 0f;

	public float logoTimeOnScreen = 3f;
	public float idleTimeOnScreen = 3f;
	public float droneSpeed = 0.1f;
	public float idleSpeed = 0.05f;
	public float showOffSpeed = 0.1f;

	public CustomEvent OnFlyToCameraComplete;
	public CustomEvent OnReturnToShowOffComplete;
	public CustomEvent OnGoToComplete;
	 
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(2f);
		//InitializeFlyToCamera();
	}

	private void Update()
	{
		switch(_currentState)
		{
			case(SateliteState.fly_to_camera):
				FlyToCamera();
			break;

			case(SateliteState.idle_flight):
				//IdleFlight();
			break;

			case(SateliteState.show_off):
				ShowOff();
			break;

			case(SateliteState.return_to_show_off):
				ReturnToShowOff();
			break;

			case(SateliteState.idle_still):
				//TODO: OR NEVER DO
			break;
		}
	}

	public void CreateSplineToPosition(Vector3 targetPosition)
	{
		if(_tempararySpline != null)
		{
			Destroy(_tempararySpline.gameObject);
			_tempararySpline = null;
		}
		GameObject newborn = new GameObject("{TEMP} - TemporarySpline");
		_tempararySpline = newborn.AddComponent<BezierCurve>();

		_tempararySpline.transform.position = transform.position;
		_tempararySpline.Reset();

		_tempararySpline.SetFinalPosition(_tempararySpline.transform.InverseTransformPoint(targetPosition));

		SetBezierSpline(_tempararySpline);
	}

	public void SetSateliteState(SateliteState state)
	{
		_currentState = state;
		OnEnterState();
	}

	private void OnEnterState()
	{
		if(_currentState == SateliteState.camera_idle)
		{
			gameObject.GetComponent<Renderer>().enabled = false;
			transform.GetChild(0).gameObject.SetActive(false);
		}else{
			gameObject.GetComponent<Renderer>().enabled = true;
			transform.GetChild(0).gameObject.SetActive(true);
		}
	}

	public void SetBezierSpline(BezierCurve spline)
	{
		_splineProgress = 0f;
		_currentSpline = spline;
	}

#region INITIALIZATION FUNCTIONS
	public void InitializeFlyToCamera()
	{
		Transform target = GameObject.FindGameObjectWithTag("CameraPosition").transform;
		CreateSplineToPosition(target.position);
		SetSateliteState(SateliteState.fly_to_camera);
	}

	public void InitializeShowOff()
	{
		SetSateliteState(SateliteState.show_off);
	}

	public void InitializeReturnToShowOff()
	{
		SetSateliteState(SateliteState.return_to_show_off);
	}
#endregion

#region STATE FUNCTIONS
	private bool _setupGoTo = false;
	private void DroneGoTo(Transform target)
	{
		if(!_setupGoTo)
		{
			CreateSplineToPosition(target.position);
			_setupGoTo = true;
		}

		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(_currentSpline.GetDirection(_splineProgress));
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				SetSateliteState(SateliteState.idle_still);
				_setupGoTo = false;
				OnGoToComplete.Invoke();
				return;
			}
		}
	}

	private void ReturnToShowOff()
	{
		if(!_setupReturn)
		{
			SetupReturn();
			_setupReturn = true;
			return;
		}

		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(_currentSpline.GetDirection(_splineProgress));
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				SetSateliteState(SateliteState.show_off);
				_setupReturn = false;
				OnReturnToShowOffComplete.Invoke();
				return;
			}
		}
	}

	private void SetupReturn()
	{
		if(_showOff == null)
		{
			_showOff = GameObject.FindGameObjectWithTag("ShowOffLoopSatelite").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		CreateSplineToPosition(_showOff.GetPoint(0f));
	}

	private void FlyToCamera()
	{
		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * droneSpeed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				OnFlyToCameraComplete.Invoke();
				SetSateliteState(SateliteState.camera_idle);
			}
		}
	}
	
	private void IdleFlight()
	{
		if(_idleLoop == null)
		{
			_idleLoop = GameObject.FindGameObjectWithTag("IdleLoopSatelite").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		_splineProgress += Time.deltaTime * idleSpeed;
		if (_splineProgress > 1f) 
		{
			_splineProgress = 0f;
		}

		Vector3 position = _idleLoop.GetPoint(_splineProgress);
		transform.position = Vector3.Lerp (transform.position, position, _splineProgress);
		
		var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);
	}

	private void ShowOff()
	{
		if(_showOff == null)
		{
			_showOff = GameObject.FindGameObjectWithTag("ShowOffLoopSatelite").GetComponent<BezierSpline>();
			_splineProgress = 0f;
		}

		_splineProgress += Time.deltaTime * showOffSpeed;
		if (_splineProgress > 1f) {
			_splineProgress = 0f;
			return;
		}

		Vector3 position = _showOff.GetPoint(_splineProgress);
        transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

		transform.LookAt(position + _showOff.GetDirection(_splineProgress));
	}
#endregion
}
