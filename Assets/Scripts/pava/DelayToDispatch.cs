﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayToDispatch : MonoBehaviour 
{
	public float delayToDispatch = 3f;
	private float _currentTime = 0f;

	public bool hasDispatched = false;
	public CustomEvent OnDelayComplete;

	private void OnEnable() 
	{
		_currentTime = 0f;
	}	

	private void Update()
	{
		if(!hasDispatched)
		{
			_currentTime += Time.deltaTime;
			if(_currentTime >= delayToDispatch)
			{
				hasDispatched = true;
				OnDelayComplete.Invoke();
			}
		}
	}
}
