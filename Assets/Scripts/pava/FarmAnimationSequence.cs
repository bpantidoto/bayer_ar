﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FarmAnimationSequence : MonoBehaviour 
{
	public Animator animator;
	public string playSequenceTrigger = "PlayAnimation";
	public string resetSequenceTrigger = "Reset";
	public string animationName = "Idle Open";

	private bool _isAnimationFinished = false;
	private bool _hasDispatchedEvent = false;
	private bool _shouldUpdate = true;

	public GameObject[] listToDeactivate;
	public GameObject[] listToActivate;

	public float sequenceStartDelay = 0.5f;

	public CustomEvent onSequenceStart;
	public CustomEvent onSequenceComplete;

	

	public void PlayAnimationSequence()
	{
		animator.SetTrigger(playSequenceTrigger);
		StartCoroutine(OnSequenceStartDelay());
	}

	private IEnumerator OnSequenceStartDelay()
	{
		yield return new WaitForSeconds(sequenceStartDelay);
		onSequenceStart.Invoke();
	}

	private void Update()
	{
		if(_shouldUpdate)
		{
			if(animator.GetCurrentAnimatorStateInfo(0).IsName(animationName))
			{
				_isAnimationFinished = true;	
			}

			if(_isAnimationFinished && !_hasDispatchedEvent)
			{
				SequenceEnd();
				return;
			}
		}
	}

	private void SequenceEnd()
	{
		_hasDispatchedEvent = true;
		_shouldUpdate = false;
		ToggleObjects();
		onSequenceComplete.Invoke();
	}

	public void ToggleObjects()
	{
		foreach (GameObject current in listToDeactivate)
		{
			current.SetActive(false);
		}

		foreach (GameObject current in listToActivate)
		{
			current.SetActive(true);
		}
	}

	public void ResetAnimationSequence()
	{
		_shouldUpdate = false;
		_hasDispatchedEvent = false;
		_isAnimationFinished = false;
		animator.SetTrigger(resetSequenceTrigger);
		
		//INVERSE
		foreach (GameObject current in listToDeactivate)
		{
			current.SetActive(true);
		}

		foreach (GameObject current in listToActivate)
		{
			current.SetActive(false);
		}
		StartCoroutine(EndReset());
	}

	private IEnumerator EndReset()
	{
		yield return new WaitForEndOfFrame();
		_shouldUpdate = true;
	}
}
