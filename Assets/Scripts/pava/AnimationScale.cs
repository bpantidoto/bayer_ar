﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimationScale : MonoBehaviour 
{
	public Ease easing;
	public float animationTime = 0.5f;
	public float delay = 0;
	private Vector3 _initialScale = new Vector3(0,0,0);
	private Vector3 _finalScale;

	public bool userRandomDelay = false;
	public float delayMin = 0;
	public float delayMax = 1f;

	private void Start() 
	{
		_finalScale = transform.localScale;
		Reset();
		SetDelay();
	}

	private void SetDelay()
	{
		if(userRandomDelay)
		{
			delay = Random.Range(delayMin, delayMax);
		}
	}

	public void Reset()
	{
		transform.localScale = _initialScale;
	}

	public void AnimateIn()
	{
		transform.DOScale(_finalScale, animationTime).SetDelay(delay).SetEase(easing);
	}
}
