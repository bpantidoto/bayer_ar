﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class LoadingGraphics : MonoBehaviour 
{
	public Image[] images;
	public float tweenTime = 0.5f;


	private float _currentTime = 1f;
	public float interval = 1f;
	private bool isVisible = true;
	private void Update()
	{
		_currentTime += Time.deltaTime;
		if(_currentTime >= interval)
		{	
			if(isVisible)
			{
				Fade(0f);
				isVisible = false;
				_currentTime = 0f;
			}else{
				Fade(1f);
				isVisible = true;
				_currentTime = 0f;
			}
		}
	}

	private void Fade(float value)
	{
		for(int i = 0; i < images.Length; i++)
		{
			images[i].DOFade(value, tweenTime).SetDelay(i*tweenTime);
		}
	}
}

