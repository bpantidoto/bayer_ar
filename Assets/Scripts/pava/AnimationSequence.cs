﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationSequence 
{
	public CustomEvent StartSequence;
	public CustomEvent ResetSequence;
	public AudioClip sequenceSound;
	public float delayToStart;
	public float delayToPlaySound;
	public bool useSoundAsDelayToAdvance;
	public bool autoCompleteSequenceWithDelay;
	public float delayToComplete;
}
