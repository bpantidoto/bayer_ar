﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TabletAnimationSequence : MonoBehaviour 
{
	public Vector3 offScreenPosition;
	public Vector3 onScreenPosition;

	public float tabletTimeOnScreen = 3f;

	public ShitExplosion shitExplosion;

	public CustomEvent OnAnimationComplete;

	public Renderer screen;

	public Material maisMelhor;
	public Material agroservice;

	public bool autoplayExplosion;
	public bool autoplayMaisMelhor;
	public bool autoplayAgroservice;

	private void Awake() 
	{
		Setup();
	}

#if UNITY_EDITOR	 
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(1f);
		if(autoplayExplosion)
			PlayAnimationShitExplosion();
		if(autoplayMaisMelhor)
			PlayAnimationMaisMelhor();
		if(autoplayAgroservice)
			PlayAnimationAgroservice();
	}
#endif

	private void Setup()
	{
		transform.position = offScreenPosition;
	}

	public void Reset()
	{
		transform.DOKill();
		StopAllCoroutines();
		Setup();
	}

	public void PlayAnimationMaisMelhor()
	{
		screen.material = maisMelhor;
		transform.DOMove(onScreenPosition, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
			//StartCoroutine(HideTablet());
		});
	}

	public void PlayAnimationAgroservice()
	{
		screen.material = agroservice;
		transform.DOMove(onScreenPosition, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
			//StartCoroutine(HideTablet());
			//OnAnimationComplete.Invoke();
		});
	}

	public void PlayAnimationShitExplosion()
	{
		screen.material = agroservice;
		transform.position = onScreenPosition;
		shitExplosion.StartExplosion();
		StartCoroutine(HideTablet());	
	}

	private IEnumerator HideTablet()
	{
		yield return new WaitForSeconds(tabletTimeOnScreen);
		transform.DOMove(offScreenPosition, 1.5f).SetEase(Ease.InBack).OnComplete(delegate{
			//OnAnimationComplete.Invoke();	
		});
	}
}
