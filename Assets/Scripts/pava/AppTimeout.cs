﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppTimeout : MonoBehaviour 
{
	public float timeoutMax = 30f;
	private float _currentTimeOut = 0f;
	private bool _isTimeRunning = false;

	public CustomEvent OnTimeout;

	private void Update()
	{
		if(_isTimeRunning)
		{
			_currentTimeOut += Time.deltaTime;
			if(_currentTimeOut >= timeoutMax)
			{
				_isTimeRunning = false;
				OnTimeout.Invoke();
				return;
			}
		}
	}
	
	public void StartTimeout()
	{
		_isTimeRunning = true;
	}

	public void StopTimeout()
	{
		_currentTimeOut = 0f;
		_isTimeRunning = false;
	}
}
