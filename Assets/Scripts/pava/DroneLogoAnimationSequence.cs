﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DroneLogoAnimationSequence : MonoBehaviour 
{
	public Vector3 offScreenPosition;
	public Vector3 onScreenPosition;
	public Vector3 onScreenPositionEndGame;

	public string triggerPlay = "PlayAnimation";
	public string triggerReset = "Reset";
	public float bannerScreenTIme = 5f;

	public float logoTimeOnScreen;

	public GameObject drone;
	public GameObject logo;
	public GameObject particles;

	public SpriteRenderer banner;
	private Animator _bannerAnim;

	private Vector3 _droneInitialScale;
	private Vector3 _logoInitialScale;

	public CustomEvent OnAnimationComplete;

	public bool autoplayLogo;
	public bool autoplayEndGame;

	private void Awake() 
	{
		_bannerAnim = banner.GetComponent<Animator>();
		_logoInitialScale = logo.transform.localScale;
		_droneInitialScale = drone.transform.localScale;
		Setup();
	}
	
#if UNITY_EDITOR
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(1f);
		if(autoplayLogo)
			PlayLogoAnimation();
		if(autoplayEndGame)
			PlayEndGameAnimation();
	}
#endif
	
	private void Setup()
	{
		particles.SetActive(false);

		drone.transform.localScale = _droneInitialScale;
		logo.transform.localScale = Vector3.zero;
		transform.position = offScreenPosition;

		banner.DOFade(0f,0f);
		_bannerAnim.SetTrigger(triggerReset);
	}

	public void Reset()
	{
		transform.DOKill();
		drone.transform.DOKill();
		logo.transform.DOKill();
		banner.DOKill();
		StopAllCoroutines();
		Setup();
	}

	public void PlayLogoAnimation()
	{
		transform.DOMove(onScreenPosition, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
			particles.SetActive(true);
			drone.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBounce);
			logo.transform.DOScale(_logoInitialScale, 1f).SetEase(Ease.OutBounce).SetDelay(0.2f).OnComplete(delegate{
				particles.SetActive(false);
				StartCoroutine(ReturnToDroneForm());
			});
		});
	}

	public void PlayEndGameAnimation()
	{
		banner.DOFade(1f, 0.5f).OnComplete(delegate{
			_bannerAnim.SetTrigger(triggerPlay);
		});
		transform.DOMove(onScreenPositionEndGame, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
			StartCoroutine(EndGame());
		});
	}

	private IEnumerator EndGame()
	{
		yield return new WaitForSeconds(bannerScreenTIme);
		OnAnimationComplete.Invoke();
	}

	private IEnumerator ReturnToDroneForm()
	{
		yield return new WaitForSeconds(logoTimeOnScreen);
		particles.SetActive(true);
		logo.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBounce);
		drone.transform.DOScale(_droneInitialScale, 1f).SetEase(Ease.OutBounce).OnComplete(delegate{
			particles.SetActive(false);
			transform.DOMove(offScreenPosition, 1.5f).SetEase(Ease.InBack).SetDelay(1.5f).OnComplete(delegate{
				OnAnimationComplete.Invoke();
			});
		});
	}

	public void PlayEndGameShowOnScreenAnimation()
	{
		transform.DOMove(onScreenPosition, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
			//TODO: EVENT?
		});
	}
}
