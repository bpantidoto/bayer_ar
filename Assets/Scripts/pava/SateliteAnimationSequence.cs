﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SateliteAnimationSequence : MonoBehaviour 
{
	public Vector3 offScreenPosition;
	public Vector3 onScreenPosition;

	public float timeOnScreen;
	public float logoAnimationDelay = 1f;
	public string triggerReset = "Reset";
	public string triggerPlay = "PlayAnimation";

	public GameObject[] particles;


	public Animator logo;
	public Transform alphaBG;
	//public Transform logo;
	public Transform hologramRaysParent;
	private Transform[] holorays;	

	public CustomEvent OnAnimationComplete;

	public bool autoplayAnimation;

	private void Awake() 
	{
		Setup();
	}
	
#if UNITY_EDITOR
	private IEnumerator Start()
	{
		yield return new WaitForSeconds(1f);
		if(autoplayAnimation)
			PlayAnimation();
	}
#endif
	
	private void Setup()
	{
		alphaBG.GetComponent<Renderer>().material.DOFade(0f,0f);
		
		transform.position = offScreenPosition;
		ToggleParticles(false);
		GetHoloRays();
		//logo.GetChild(0).GetComponent<Renderer>().material.DOFade(0f,0f);
		logo.gameObject.SetActive(false);
		logo.SetTrigger(triggerReset);
	}

	public void Reset()
	{
		transform.DOKill();
		StopAllCoroutines();
		Setup();
	}

	public void PlayAnimation()
	{
		alphaBG.GetComponent<Renderer>().material.DOFade(1f,0.5f).OnComplete(delegate{
				transform.DOMove(onScreenPosition, 3f).SetEase(Ease.OutBack).OnComplete(delegate{
				ToggleParticles(true);
				AnimateHolorays();
				StartCoroutine(AnimateLogo());
				StartCoroutine(ReturnToOriginalPlace());
			});
		});
	}

	private IEnumerator AnimateLogo()
	{
		yield return new WaitForSeconds(logoAnimationDelay);
		logo.gameObject.SetActive(true);
		logo.SetTrigger(triggerPlay);
	}

	private IEnumerator ReturnToOriginalPlace()
	{
		yield return new WaitForSeconds(logoAnimationDelay+timeOnScreen);
		DeactivateHologram();
		transform.DOMove(offScreenPosition, 1.5f).SetEase(Ease.InBack).SetDelay(1.5f).OnComplete(delegate{
			OnAnimationComplete.Invoke();
		});
	}

	private void ToggleParticles(bool active)
	{
		foreach(GameObject current in particles)
		{
			current.SetActive(active);
		}
	}

	private void GetHoloRays()
	{
		holorays = new Transform[hologramRaysParent.childCount];
		for(int i = 0; i < hologramRaysParent.childCount; i++)
		{
			holorays[i] = hologramRaysParent.GetChild(i);
			holorays[i].localScale = Vector3.zero;
		}
	}

	private void AnimateHolorays()
	{
		foreach (Transform current in holorays)
		{
			current.DOScale(new Vector3(1f,1f,1f), 0.7f).SetDelay(Random.Range(0.1f,1f));
		}
	}

	private void DeactivateHologram()
	{
		foreach (Transform current in holorays)
		{
			current.DOScale(Vector3.zero, 0.3f);
		}
		ToggleParticles(false);
		alphaBG.GetComponent<Renderer>().material.DOFade(0f,0.5f);
		logo.gameObject.SetActive(false);
	}
}

