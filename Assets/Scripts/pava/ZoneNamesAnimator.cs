﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class ZoneNamesAnimator : MonoBehaviour 
{	
	public float animationTime = 0.3f;
	public float delayToExit = 3f;
	public float delayToEnter = 1f;

	public Ease animateInEase;
	public Ease animateOutEase;

	public CustomEvent onSequenceComplete;

	public bool isDebug = false;
	public bool autoPlay = false;
	public bool useTween = false;

	private void Awake()
	{
		if(!isDebug)
		{
			Deactivate();
		}
	}

	private void Deactivate()
	{
		for(int i = 0; i < transform.childCount; i++)
		{
			transform.GetChild(i).gameObject.SetActive(false);
		}
	}
 
	private IEnumerator Start () 
	{
		yield return new WaitForSeconds(1f);
		if(autoPlay)
		{
			Deactivate();
			PlayAnimationSequenceEnter();
		}
	}
	

	public void PlayAnimationSequenceEnter()
	{
		StartCoroutine(Enter());
	}

	public void Reset()
	{
		StopAllCoroutines();
		for(int i = 0; i < transform.childCount; i++)
		{
			if(useTween)
			{
				transform.GetChild(i).DOKill();
				transform.GetChild(i).DOScale(Vector3.zero, 0f).SetEase(animateOutEase).SetDelay(animationTime*i);
			}else{
				transform.GetChild(i).gameObject.SetActive(false);
			}
		}
	}

	public IEnumerator Enter()
	{
		yield return new WaitForSeconds(delayToEnter);
		for(int i = 0; i < transform.childCount; i++)
		{
			if(useTween)
			{
				transform.GetChild(i).DOScale(new Vector3(1f,1f,1f), animationTime).SetEase(animateInEase).SetDelay(animationTime*i);
			}else{
				StartCoroutine(DelayedActivation(animationTime*i, transform.GetChild(i).gameObject));
			}
		}
		StartCoroutine(PlayAnimationSequenceExit());
	}

	private IEnumerator DelayedActivation(float delay, GameObject target, bool toggle = true)
	{
		yield return new WaitForSeconds(delay);
		target.SetActive(toggle);
	}

	private IEnumerator PlayAnimationSequenceExit()
	{
		yield return new WaitForSeconds(delayToExit);
		for(int i = 0; i < transform.childCount; i++)
		{
			
			transform.GetChild(i).DOScale(Vector3.zero, animationTime).SetEase(animateOutEase).SetDelay(animationTime*i);
			/* 
			if(useTween)
			{
			}else{
				StartCoroutine(DelayedActivation(animationTime*i, transform.GetChild(i).gameObject, false));	
			}
			*/
		}
		yield return new WaitForSeconds(animationTime*3f);
		onSequenceComplete.Invoke();
	}
}
