﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HUDController : MonoBehaviour 
{
	public float animationTime = 3f;
	public float introduceDelayOnScreen = 2f;
	public float[] readingTimes;

	public HudAnimationsController introduction;
	public HudAnimationsController soja;
	public HudAnimationsController milho;
	public HudAnimationsController[] textAnimations;

	public HudAnimationsController[] baloonTextAnimations;
	public string[] baloonContent;

	public string[] content;

	public void PlayIntroduceSequence()
	{
		introduction.PlayAnimation();
		StartCoroutine(OnCompleteIntroduceCoroutine());
	}

	public void PlaySojaSequence()
	{
		soja.PlayAnimation();
	}

	public void PlayMilhoSequence()
	{
		milho.PlayAnimation();
	}

	private IEnumerator OnCompleteIntroduceCoroutine()
	{
		yield return new WaitForSeconds(animationTime+introduceDelayOnScreen);
		ResetHud();
	}

	public void ResetHud()
	{
		introduction.Reset();
		soja.Reset();
		milho.Reset();
		foreach (HudAnimationsController current in textAnimations)
		{
			current.Reset();
		}
		foreach (HudAnimationsController current in baloonTextAnimations)
		{
			current.Reset();
		}
	}

	public void ResetAllButSoy()
	{
		introduction.Reset();
		milho.Reset();
		foreach (HudAnimationsController current in textAnimations)
		{
			current.Reset();
		}
		foreach (HudAnimationsController current in baloonTextAnimations)
		{
			current.Reset();
		}
	}

	public void ResetAllButCorn()
	{
		introduction.Reset();
		soja.Reset();
		foreach (HudAnimationsController current in textAnimations)
		{
			current.Reset();
		}

		foreach (HudAnimationsController current in baloonTextAnimations)
		{
			current.Reset();
		}
	}

	public bool resetHudOnTextChange;
	public void SetText(int textIndex)
	{
		if(resetHudOnTextChange)
		{
			for(int i = 0; i < textAnimations.Length; i++)
			{
				if(i != textIndex)
				{
					textAnimations[i].Reset();
				}
			}
		}
		textAnimations[textIndex].PlayAnimation();
		textAnimations[textIndex].SetText(content[textIndex]);
	}

	public int[] soyTexts;
	public int[] cornTexts;
	public void SetBaloonText(int textIndex)
	{
		if(resetHudOnTextChange)
		{
			for(int i = 0; i < baloonTextAnimations.Length; i++)
			{
				if(i != textIndex)
				{
					baloonTextAnimations[i].Reset();
				}
			}
		}
		baloonTextAnimations[textIndex].PlayAnimation();
		baloonTextAnimations[textIndex].SetText(baloonContent[textIndex]);

		PlaySoyOrCornAnimation(textIndex);
	}

	private void PlaySoyOrCornAnimation(int index)
	{
		foreach(int current in soyTexts)
		{
			if(current == index)
			{
				PlaySojaSequence();
			}
		}

		foreach(int current in cornTexts)
		{
			if(current == index)
			{
				PlayMilhoSequence();
			}
		}
	}
}


