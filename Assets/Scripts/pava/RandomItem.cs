﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RandomItem : MonoBehaviour 
{
	public GameObject[] possibilities;
	public GameObject particle;
	public Transform sphere;
	public Material[] materials;

	public float scaleDuration = 0.5f;
	public Ease easing;

	private Vector3 _sphereInitialScale;
	private int _randomizedItem;
	private int _randomizedMaterial;


	private void Awake()
	{
		Randomize();
		Setup();
	}

	public void PlayAnimation()
	{
		sphere.DOScale(_sphereInitialScale, scaleDuration).SetEase(easing).OnComplete(delegate{
			TurnIntoItem();
		});
	}

	private void TurnIntoItem()
	{
		sphere.gameObject.SetActive(false);
		particle.SetActive(true);
		possibilities[_randomizedItem].SetActive(true);
	}

	private void Setup()
	{
		_sphereInitialScale = sphere.localScale;
		sphere.localScale = Vector3.zero;

		foreach (GameObject current in possibilities)
		{
			current.SetActive(false);
		}
		particle.SetActive(false);
		sphere.GetComponent<Renderer>().material = materials[_randomizedMaterial];
	}

	private void Randomize()
	{
		_randomizedItem = Random.Range(0, possibilities.Length);
		_randomizedMaterial = Random.Range(0, materials.Length);	
	}


	private void TestRandomization()
	{
		for(int i =0;i  < 200; i++)
		{
			Randomize();
			if(_randomizedItem == 0)
			{
				Debug.Log("SIM");
			}
		}
	}
}

