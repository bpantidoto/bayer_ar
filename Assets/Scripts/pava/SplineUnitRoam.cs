﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SplineUnitRoam : MonoBehaviour 
{
	public BezierCurve spline;

	public bool isPaused = false;
	public bool inverse = false;
	public bool useLoop = false;
	public bool walkToBeginAndWait = false;

	private bool _isReady = false;

	private float _splineProgress = 0f;
	public float speed = 0.5f;

	public void PlaySequence()
	{
		isPaused = false;
	}

	public void Awake()
	{
		walkToBeginAndWait = true;
		Vector3 position = spline.GetPoint(0f);

		transform.position = position;
		_isReady = true;
	}

	public void ResetToLastCheckPoint()
	{
		Vector3 position = spline.GetPoint(_splineProgress);
		transform.position = position;
		transform.LookAt(position + spline.GetDirection(_splineProgress));
		isPaused = true;
	}
	
	private void Update()
	{
		if(!_isReady)
		{
			//Debug.Log("NOT READY");
			return;
		}
			
		if(isPaused)
		{
			//Debug.Log("PAUSED");
			return;
		}

		_splineProgress += Time.deltaTime*speed;
		Vector3 position = spline.GetPoint(_splineProgress);
		transform.position = position;
		transform.LookAt(position + spline.GetDirection(_splineProgress));	

		if(_splineProgress >= 1f)
		{
			isPaused = true;
		}
	}
}
