﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class TrackableEventHandler : MonoBehaviour, ITrackableEventHandler 
{
    private TrackableBehaviour _mTrackableBehaviour;

	public CustomEvent onTrackingFound;
	public CustomEvent onTrackingLost;

    protected virtual void Start()
    {
        _mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (_mTrackableBehaviour)
            _mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected virtual void OnDestroy()
    {
        if (_mTrackableBehaviour)
            _mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
	{
        if (newStatus == TrackableBehaviour.Status.DETECTED || newStatus == TrackableBehaviour.Status.TRACKED || newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED && newStatus == TrackableBehaviour.Status.NOT_FOUND)
        {
            OnTrackingLost();
        }
        else
        {
            OnTrackingLost();
        }
    }

    public float minimumTimeBeforeDispatch = 0.3f;
    private float _timeSinceLastTracked = 0f;
    private bool _hasTracked = false;
    private bool _shouldUpdate = true;
    private void Update()
    {
        if(_shouldUpdate)
        {
            if(_hasTracked)
            {
                _timeSinceLastTracked = 0f;
            }else{
                _timeSinceLastTracked += Time.deltaTime;
                if(_timeSinceLastTracked >= minimumTimeBeforeDispatch)
                {
                    onTrackingLost.Invoke();
                    _shouldUpdate = false;
                }
            }
        }
    }

    private void OnTrackingFound()
    {
        onTrackingFound.Invoke();
        _shouldUpdate = true;
        _hasTracked = true;
    }

    private void OnTrackingLost()
    {
        _hasTracked = false;
    }
}
