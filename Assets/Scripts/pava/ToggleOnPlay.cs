﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleOnPlay : MonoBehaviour 
{
	public bool correctActive;
	
	private void Awake()
	{
		gameObject.SetActive(correctActive);
	}
}
