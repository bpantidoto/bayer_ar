﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlantPoisitioner : MonoBehaviour 
{
	private BezierCurve _spline;
	public Ease animationEase;
	public float animationTime = 0.3f;
	
	public void PlayAnimationSequence()
	{
		_spline = GetComponent<BezierCurve>();

		float percent = 1f/transform.childCount;
		for(int i = 0; i<transform.childCount; i++)
		{
			float progress = i * percent;
			Transform current = transform.GetChild(i);
			Vector3 position = _spline.GetPoint(progress);
			Vector3 lastScale = current.localScale;
			current.position = position;

			//TODO: PUT A SEQUENCE HERE
			 
			current.localScale = new Vector3(0f,0f,0f);
			current.DOScale(lastScale, animationTime).SetDelay(progress).SetEase(animationEase);
			
		}
	}
}
