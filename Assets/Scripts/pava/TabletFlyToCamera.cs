﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public enum TabletState{
	fly_to_camera,
	idle
}

public class TabletFlyToCamera : MonoBehaviour 
{
	private BezierCurve _tempararySpline;
	private BezierCurve _currentSpline;

	private TabletState _currentState;

	private float _splineProgress = 0f;

	public float speed = 0.3f;


	public CustomEvent OnFlyToCameraComplete;

	private IEnumerator Start()
	{
		yield return new WaitForSeconds(2f);
	}

	private void Update()
	{
		switch(_currentState)
		{
			case(TabletState.fly_to_camera):
				FlyToCamera();
			break;
		}
	}

	public void CreateSplineToPosition(Vector3 targetPosition)
	{
		if(_tempararySpline != null)
		{
			Destroy(_tempararySpline.gameObject);
			_tempararySpline = null;
		}
		GameObject newborn = new GameObject("{TEMP} - TemporarySpline");
		_tempararySpline = newborn.AddComponent<BezierCurve>();

		_tempararySpline.transform.position = transform.position;
		_tempararySpline.Reset();

		_tempararySpline.SetFinalPosition(_tempararySpline.transform.InverseTransformPoint(targetPosition));

		SetBezierSpline(_tempararySpline);
	}

	public void SetState(TabletState state)
	{
		_currentState = state;
	}

	public void SetBezierSpline(BezierCurve spline)
	{
		_splineProgress = 0f;
		_currentSpline = spline;
	}

#region INITIALIZATION FUNCTIONS
	public void InitializeFlyToCamera()
	{
		Transform target = GameObject.FindGameObjectWithTag("CameraPosition").transform;
		CreateSplineToPosition(target.position);
		SetState(TabletState.fly_to_camera);
	}
#endregion

#region STATE FUNCTIONS
	private void FlyToCamera()
	{
		if(_currentSpline != null)
		{
			_splineProgress += Time.deltaTime * speed;
			Vector3 position = _currentSpline.GetPoint(_splineProgress);
			transform.position = Vector3.Lerp (transform.position, position, _splineProgress);

			var targetRotation = Quaternion.LookRotation(Camera.main.transform.position - transform.position);
			transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 5 * Time.deltaTime);

			if(_splineProgress >= 1f)
			{
				OnFlyToCameraComplete.Invoke();
				SetState(TabletState.idle);
			}
		}
	}
#endregion
}
