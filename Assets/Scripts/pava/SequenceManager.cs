﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SequenceManager : MonoBehaviour 
{
	public int debugSequence = 0;
	public bool autoStart = false;
	public CustomEvent DefaultResetAction;
	public CustomEvent DefaultSequenceAction;
	public AnimationSequence[] sequences;

	private int _currentSequence = 0;
	private bool[] _sequenceCompletion;

	private SoundManager _soundManager;
	private Transform _cameraTransform;

	private void Awake()
	{
		_sequenceCompletion = new bool[sequences.Length];
		_soundManager = GetComponent<SoundManager>();
		_cameraTransform = Camera.main.transform;
		DOTween.SetTweensCapacity(1000,100);
	}

#if UNITY_EDITOR
	private void Start()
	{
		_currentSequence = debugSequence;
		if(autoStart)
		{
			PlayCurrentSequence();
		}
	}
	
	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.P))
		{
			//Debug.Log("STOP SEQUENCE");
			StopSequence();
		}
		if(Input.GetKeyDown(KeyCode.O))
		{
			//Debug.Log("PLAY SEQUENCE");
			PlayCurrentSequence();
		}
	}
#endif
	
	public void PlayCurrentSequence()
	{
		DefaultSequenceAction.Invoke();
		StartCoroutine(PlaySequence());
		StartCoroutine(PlaySequenceSound());
		if(sequences[_currentSequence].useSoundAsDelayToAdvance)
		{
			StartCoroutine(SoundAdvance());
		}else if(sequences[_currentSequence].autoCompleteSequenceWithDelay)
		{
			StartCoroutine(AutoCompleteSequence());
		}
	}

	private IEnumerator PlaySequence()
	{
		yield return new WaitForSeconds(sequences[_currentSequence].delayToStart);
		sequences[_currentSequence].StartSequence.Invoke();
	}

	private IEnumerator PlaySequenceSound()
	{
		float delay = 0;
		if(_currentSequence < sequences.Length)
		{
			delay = sequences[_currentSequence].delayToPlaySound;
		}
		yield return new WaitForSeconds(delay);
		AudioClip soundToPlay = sequences[_currentSequence].sequenceSound;
		if(soundToPlay != null)
		{
			_soundManager.PlaySound(soundToPlay, _cameraTransform.position);
		}

		_soundManager.PlayBackgroundMusic();
	}

	public void StopSequence()
	{
		StopAllCoroutines();
		ResetSequence();
	}

	private void ResetSequence()
	{
		DefaultResetAction.Invoke();
		if(_currentSequence < sequences.Length)
		{
			sequences[_currentSequence].ResetSequence.Invoke();
		}
		_soundManager.StopCurrentPlayingSound();
		_soundManager.StopBackGroundMusic();
	}

	public void NextSequence()
	{
		_sequenceCompletion[_currentSequence] = true;
		_currentSequence++;

		if(_currentSequence < sequences.Length)
		{
			PlayCurrentSequence();
		}else{
			bool isComplete = true;
			foreach(bool current in _sequenceCompletion)
			{
				if(!current)
				{
					isComplete = false;
				}
			}
			if(isComplete)
			{
				Debug.Log("END GAME");
			}
		}
	}

	private IEnumerator AutoCompleteSequence()
	{
		yield return new WaitForSeconds(sequences[_currentSequence].delayToComplete);
		NextSequence();
	}

	private IEnumerator SoundAdvance()
	{
		yield return new WaitForSeconds(sequences[_currentSequence].sequenceSound.length);
		NextSequence();
	}
}
